# https://atrium.ai/resources/build-and-deploy-a-docker-containerized-python-machine-learning-model-on-heroku/

# First, load the dataset from sklearn.

from  sklearn import  datasets

iris = datasets.load_iris()

# Separate features and target labels in different data frames.

x = iris.data
y = iris.target

from sklearn.model_selection import train_test_split

x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=.3)

# Build the model using KNeighborsClassifier.

from sklearn import neighbors

knn = neighbors.KNeighborsClassifier()

# Train the model and then do prediction on the test dataset and calculate the accuracy of the model.

knn.fit(x_train,y_train)

predictions = knn.predict(x_test)

from sklearn.metrics import accuracy_score

print(accuracy_score(y_test,predictions))

#Now, we will export the model coefficients in the pickle file

import pickle

with open('./model.pkl', 'wb') as model_pkl:
  pickle.dump(knn, model_pkl)

 
